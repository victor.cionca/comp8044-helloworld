#include "contiki.h"
#include "net/netstack.h"
#include "net/nullnet/nullnet.h"

#include "lib/random.h"
#include <string.h>
#include <stdio.h>

#include "sys/log.h" // this is for LOG_INFO macros
#define LOG_MODULE "Test"
#define LOG_LEVEL LOG_LEVEL_INFO

PROCESS (n2ncomms, "N2N Comms");
AUTOSTART_PROCESSES (&n2ncomms);

enum{
    PKT_START = 0,
    PKT_ID,
};

// Define a structure to hold the contents of the packet
struct start_pkt{
    uint8_t type;
    uint8_t counter;
}; // This initializes the protocol

struct id_pkt{
    uint8_t type;
    uint8_t counter;
};    // Sent by nodes, contains the node id

struct schedule_pkt{  // Sent by gateway
    uint8_t type;
    // time offset
    // length of the schedule frame (number of slots)
    // actual schedule - an array of node ids
};

struct data_pkt{    // Sent by nodes
    uint8_t type;
    // data
};

struct echo_pkt{
    uint8_t type;   // 0xAA for direct packets; 0xBB for echoes
    uint8_t src_id;
    uint8_t counter;
    uint8_t contents[10];
};

static uint8_t received = 0;
uint8_t rx_buffer[sizeof(struct echo_pkt)];
linkaddr_t src;

void nullnet_receive (
        const void *data,       // Pointer to the data received
        uint16_t len,           // The length of the data received
        const linkaddr_t *src,  // Address of the sender
        const linkaddr_t *dest) // Our address
{
    struct echo_pkt *pkt;
    uint8_t packet_type;
    packet_type = ((uint8_t*)data)[0];

    switch (packet_type){
        case PKT_START:
            {
                // cast the packet to the relevant structure
                // nodes process the packet: print that they received a start
                // gateway ignores it
                break;
            }
        case PKT_ID:
            {
                // cast the packet to the relevant structure
                // gateway processes the id packet: print that it received id
                // other nodes ignore it
                break;
            }
        default:
            break;
    }


}

PROCESS_THREAD (n2ncomms, ev, data)
{
    // All of this executed every time a new event is received.
    // Also variables here will be visible to the entire thread.
    // Static variables maintain their value between function calls.
    static struct etimer periodic_timer;
    static uint8_t count = 0;
    static struct start_pkt start_pkt;
    static struct id_pkt id_pkt;


    PROCESS_BEGIN ();

    start_pkt.type = PKT_START;
    id_pkt.type = PKT_ID;

    // Setup a callback for receiving nullnet packets
    nullnet_set_input_callback (nullnet_receive);


    // Same thing for all other instructions included here
    etimer_set(&periodic_timer,
            CLOCK_SECOND + (random_rand()%(CLOCK_SECOND>>3)));

    NETSTACK_NETWORK.output(NULL);
    if (linkaddr_node_addr.u8[0] == 1){
        while (1) { // Loop forever
            uint8_t modulo = count % 2;

            // Wait for the timer to expire and generate an event
            PROCESS_WAIT_EVENT_UNTIL (etimer_expired(&periodic_timer));

            switch (modulo){
                case PKT_START:
                    {
                        nullnet_buf = &start_pkt;
                        nullnet_len = sizeof(struct start_pkt);
                        start_pkt.counter = count;
                        break;
                    }
                case PKT_ID:
                    {
                        nullnet_buf = &id_pkt;
                        nullnet_len = sizeof(struct id_pkt);
                        id_pkt.counter = count;
                        break;
                    }

                    NETSTACK_NETWORK.output(NULL);  // broadcast

                    LOG_INFO ("Packet sent\n");
            }

            count ++;
            etimer_reset(&periodic_timer);
        }
    }
    PROCESS_END ();
}
