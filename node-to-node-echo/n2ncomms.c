#include "contiki.h"
#include "net/netstack.h"
#include "net/nullnet/nullnet.h"

#include "lib/random.h"
#include <string.h>
#include <stdio.h>

#include "sys/log.h" // this is for LOG_INFO macros
#define LOG_MODULE "Test"
#define LOG_LEVEL LOG_LEVEL_INFO

PROCESS (n2ncomms, "N2N Comms");
AUTOSTART_PROCESSES (&n2ncomms);

// Define a structure to hold the contents of the packet
struct echo_pkt{
    uint8_t type;   // 0xAA for direct packets; 0xBB for echoes
    uint8_t src_id;
    uint8_t counter;
    uint8_t contents[10];
};

static uint8_t received = 0;
uint8_t rx_buffer[sizeof(struct echo_pkt)];


// This is an example of a callback that can be used to send an echo
//void send_echo(void *arg)
//{
//    linkaddr_t dest;
//
//    // Prepare buffer for nullnet transmission
//    nullnet_buf = rx_buffer;
//    nullnet_len = sizeof(struct echo_pkt);
//
//    memset(dest.u8, 0, 8);
//    dest.u8[0] = 0; // Get the src_id field from the packet that is in the buffer
//    NETSTACK_NETWORK.output(src);
//
//}

void nullnet_receive (
        const void *data,       // Pointer to the data received
        uint16_t len,           // The length of the data received
        const linkaddr_t *src,  // Address of the sender
        const linkaddr_t *dest) // Our address
{
    struct echo_pkt *pkt;

    // If the packet is not an echo discard it
    if (((uint8_t*)data)[0] != 0xAA) return;

    // Cast the incoming packet to an echo packet
    pkt = (struct echo_pkt*)data;

    LOG_INFO ("Received from %u: %u/%u\n",// LOG_INFO is similar to printf
               pkt->src_id, pkt->counter, received);  
    received ++;

    /*
     * If this node originated the transmission
     * it must not send the echo back, because that would lead
     * to an infinite loop of echos.
     */
    if (linkaddr_node_addr.u8[0] == pkt->src_id){
        LOG_INFO ("Dropping packet\n");
        return;
    }

    /* Otherwise send an echo */

    // Prepare buffer for nullnet transmission
    // Comment the next two out when using ctimer
    nullnet_buf = rx_buffer;
    nullnet_len = sizeof(struct echo_pkt);

    // copy into the buffer the received data (data)
    memcpy(rx_buffer, data, nullnet_len);

    // Comment the next line when using ctimer
    NETSTACK_NETWORK.output(src);
    //ctimer_set(timer, random delay, send_echo, NULL);
}

PROCESS_THREAD (n2ncomms, ev, data)
{
    // All of this executed every time a new event is received.
    // Also variables here will be visible to the entire thread.
    // Static variables maintain their value between function calls.
    static struct etimer periodic_timer;
    static uint8_t count = 0;
    static linkaddr_t dest_addr;
    static struct echo_pkt pkt; 

    PROCESS_BEGIN ();

    // Variables declared here will be visible to the whole thread
    // ONLY on first run

    // We set the destination address to our address + 1
    memcpy(&dest_addr, &linkaddr_node_addr, sizeof(linkaddr_t));
    dest_addr.u8[0] += 1;

    // Setup the nullnet buffer (the packet buffer)
    nullnet_buf = (uint8_t*)&pkt;
    nullnet_len = sizeof(struct echo_pkt);

    // Put some data into the buffer
    pkt.type = 0xAA;
    pkt.src_id = linkaddr_node_addr.u8[0]; // source is first byte in our addr
    pkt.counter = count;
    strcpy((char*)(pkt.contents), "TSCHRulz"); // Some filler in the remainder
    pkt.contents[8] = 0; // Null terminate the string

    // Setup a callback for receiving nullnet packets
    nullnet_set_input_callback (nullnet_receive);


    // Same thing for all other instructions included here
    etimer_set(&periodic_timer,
            CLOCK_SECOND + (random_rand()%(CLOCK_SECOND>>3)));

    NETSTACK_NETWORK.output(&dest_addr);
    while (1) { // Loop forever
        // Wait for the timer to expire and generate an event
        PROCESS_WAIT_EVENT_UNTIL (etimer_expired(&periodic_timer));

        // Send the nullnet buffer directly to MAC
        // Parameter is destination address
        // Only nodes with an odd ID value transmit data
        if (linkaddr_node_addr.u8[0] == 1){
            //NETSTACK_NETWORK.output(&dest_addr);    // unicast
            NETSTACK_NETWORK.output(NULL);  // broadcast

            LOG_INFO ("Sent to ");
            LOG_INFO_LLADDR (&dest_addr);
            LOG_INFO_("\n");
        }

        pkt.counter++;
        etimer_reset(&periodic_timer);
    }
    PROCESS_END ();
}
